# Liara with Gitlab CI

If you push a code to this repoitory, it will deploy it to the Liara automatically.
Remember that you have to specify your project name in `.gitlab-ci.yml` file.

And also you have to go to the Settings > CI / CI > Variables and define this variable: `LIARA_TOKEN`. You can obtain the value from https://console.liara.ir/API